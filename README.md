
DLP Protector:

Protector Prerequisites Role: redhat-protector-Prerequisites

Following task will be performed by the playbook

1. Stop Firewall and disable from boot
2. Ansible pexpect module installation (its required for automate interactive mode of installation in remote machine)
3. Check the windows machine is reachable to start the installation
4. Check two NIC  cards available to processed the installation
5. check /opt partition have minimum 45 GB of available space
6. set hostname and fully qualified name are identical 
